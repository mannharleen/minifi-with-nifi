# minifi with nifi

This repo contains a docker compose file to run both nifi and minifi.
I am building this repo as a convinience to work on "nifi as a self service" for non-developers tech savy business users

### Compelling stuff:
- using mannharleen/nifi docker image which is actually a base image to load an existing volume that contins nifi
- minifi is configured to use RestChangeIngestor whereby, minifi flow can be dynamically changed using the bootstrap.conf


### For testing RestChangeIngestor use:
curl --request POST --data-binary "@config.yml" http://[IPaddress]:8338/
